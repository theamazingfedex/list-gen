# List Generator
 **Want a list? Click a button. It's that easy. Or at least it should be.**

## What it is
 As the description kindly put, it makes lists. Numbers, strings, random all
 the way. Sorted even, if you like, but good for testing functions that need a
 collection of data to churn through. Output is in JSON format, so should be
 pasteable in a good number of places. Advanced mode will have a custom syntax
 for defining random JSON objects.

## Technologies
 Some of the tools that we will be using for this project include:
 * React
 * Redux
 * Babel
 * Webpack
 * Hot-reloading (sexy dev-time)
 * Rx.js (idk how, but we should try to find a way to use it cuz it's awesome)

## Startup
 Clone this repo and from the command line, execute: `npm install` and `npm start` to start up the webpack-dev-server.
 We don't yet have a production server set up, but if you want to work on that, make a pull request! :)
 
## Contribution Guidelines
 Airbnb.com has a really great development environment, and follows a lot of good best-practices that we should also follow. Read up on the [Javascript Style Guide](https://github.com/airbnb/javascript) and code nicely!
 Also, any work that you want to do, simply create a branch off of the 'dev' branch, and create pull requests for anything you want to contribute. Make sure to document what it is that you are contributing in your pull request.

## Contributors
 * Daniel Wood - @TheAmazingFedex
 * Keenen Ruple - @KeenSouthPaw
