import React, { Component } from 'react';

export default class SimpleButton extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const buttonStyle = {
      height: this.props.height || '50px',
      width: this.props.width || '100px',
      borderRadius: this.props.borderRadius || 'none',
      color: this.props.color || 'black'
    }
    return (
      <input type="button" ref="button"
        value={ this.props.text }
        onClick={ this.props.onClick }
        style={ buttonStyle }/>
    );
  }
}
SimpleButton.PropTypes = {
  text: React.PropTypes.string.required,
  onClick: React.PropTypes.func.required,
  width: React.PropTypes.string,
  height: React.PropTypes.string
}
