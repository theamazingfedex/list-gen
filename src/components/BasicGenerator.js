import React, { Component } from 'react';
import SimpleButton from './buttons/SimpleButton';

export default class BasicGenerator extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return <div ref="basicGenerator">
             <table>
               <thead>
                 <tr>
                   <td colspan="2">
                     Basic List Generator
                   </td>
                 </tr>
                 <tr>
                   <td>
                     <strong>List-Count:</strong>
                   </td>
                   <td>
                     <input type="number" value="20" />
                   </td>
                 </tr>
                 <tr>
                   <td>All Random</td>
                   <td>Sorted</td>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <td>
                     <SimpleButton text="Numbers" />
                   </td>
                   <td>
                     <SimpleButton text="Numbers" onClick={ () => { alert('one') }} />
                   </td>
                 </tr>
                 <tr>
                   <td>
                     <SimpleButton text="Strings" />
                   </td>
                   <td>
                     <SimpleButton text="Strings" />
                   </td>
                 </tr>
                 <tr>
                   <td>
                     <SimpleButton text="Words" />
                   </td>
                   <td>
                     <SimpleButton text="Words" />
                   </td>
                 </tr>
               </tbody>
             </table>
           </div>
  }
}
