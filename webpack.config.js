var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: [
    'webpack-dev-server/client?http://0.0.0.0:3000',
    'webpack/hot/only-dev-server',
    './src/main.js'
  ],
  output: {
    path: __dirname,
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    loaders: [
      { test: path.join(__dirname, 'src'),
        loaders: ['react-hot', 'babel-loader'] }
    ]
  }
};

// module.exports = {
  // entry: [
    // 'webpack-dev-server/client?http://0.0.0.0:3000',
    // 'webpack/hot/only-dev-server',
    // './main.js',
    // './index.html'
  // ],
  // output: { path: path.join(__dirname, 'public'), filename: 'bundle.js' },
  // resolve: {
    // extensions: ['', '.js', '.jsx']
  // },
  // plugins: [
    // new webpack.HotModuleReplacementPlugin()
  // ],
  // module: {
    // loaders: [
      // {
        // loaders: ['react-hot', 'babel-loader'],
        // include: path.join(__dirname, 'src'),
        // exclude: ['/node_modules/', '/bin/'],
        // query: {
          // presets: ['es2015', 'react']
        // }
    // },
    // {
      // test: /\.html$/,
      // loader: 'file?name=[name].[ext]'
    // }
    // ]
  // }
// };
